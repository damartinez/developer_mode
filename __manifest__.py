{
    'name': 'Automatic Developer Mode',
    'summary': '''Automatically Activate Developer Mode''',
    'version': '1.0.0',
    'author': 'Dixon Martinez',
    'website': 'https://gitlab.com',
    'category': 'Tools',
    'depends': [
        'base',
        'base_setup',
    ],
    'data': [
        'views/users.xml',
        'views/developer_mode_view.xml'
    ],
    'application': False,
    'installable': True,
    'auto_install': False,
    'license': 'LGPL-3',
}



