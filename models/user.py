from odoo import models, fields

class ResUsers(models.Model):
    _inherit = 'res.users'

    debug_mode = fields.Selection(
        selection=[
            ('1', 'Debug'),
            ('assets', 'Debug with Assets'),
            ('tests', 'Debug with Assets and Tests'),
        ],
        string="Debug"
    )
