from odoo.addons.web.controllers import main as web
from odoo import http
from odoo.http import request


class DeveloperMode(web.Home):

    @http.route('/web', type='http', auth="none")
    def web_client(self, s_action=None, **kw):
        res = super(DeveloperMode, self).web_client(s_action, **kw)
        user = request.env.user
        debug_mode = kw.get('debug') if kw.get('debug') or kw.get('debug') == '' else user.debug_mode
        if debug_mode:
            request.session['debug'] = debug_mode
        return res
